odoo.define('l10n_co_edi_jorels_pos', function(require) {
    "use strict";

    var PosDB = require('point_of_sale.DB');
    var core = require('web.core');
    var module = require('point_of_sale.models');
    var gui = require('point_of_sale.gui');
    var screens = require('point_of_sale.screens');
    var _t = core._t;
    var rpc = require('web.rpc');

    var models = module.PosModel.prototype.models;

    var partner_fields = [
        'is_company',
        'l10n_co_document_type',
        'type_regime_id',
        'type_liability_id',
        'municipality_id',
        'email_edi'
    ];

    models.push(
        {
            model:  'l10n_co_edi_jorels.type_regimes',
            fields: ['name'],
            loaded: function(self, type_regimes) {
                self.type_regimes = type_regimes;
            }
        },
        {
            model:  'l10n_co_edi_jorels.type_liabilities',
            fields: ['name'],
            loaded: function(self, type_liabilities) {
                self.type_liabilities = type_liabilities;
            }
        },
        {
            model:  'l10n_co_edi_jorels.municipalities',
            fields: ['name'],
            loaded: function(self, municipalities) {
                self.municipalities = municipalities;
            }
        },
        // Por ahora solo se permiten contactos de Colombia
        // TODO: Dar soporte para otros paises
        {
            model:  'res.country.state',
            fields: ['name','country_id'],
            domain: [['country_id.name','=','Colombia']],
            loaded: function(self, states) {
                self.states = states;
            }
        },
    );

    var set_fields_to_model = function(fields, models) {
        for(var i = 0; i < models.length; i++) {
            if(models[i].model == 'res.partner') {
                var model = models[i];
                for(var j = 0; j < fields.length; j++){
                    model.fields.push(fields[j]);
                }
                var old_domain = model.domain;
                model.domain = function(self) {
                    return ['|', old_domain[0], ['id','=',self.company.partner_id[0]]];
                }

                var old_loaded = model.loaded;
                model.loaded = function(self, partners) {
                    for(var i = 0; i < partners.length; i++) {
                        if(partners[i].id == self.company.partner_id[0]) {
                            var company_partner = partners.splice(i, 1);
                            self.company_partner = company_partner;
                        }
                    }
                    old_loaded(self, partners);
                }
            }
        }
    }

    set_fields_to_model(partner_fields, models);

    screens.ClientListScreenWidget.include({
        display_client_details: function(visibility,partner,clickpos) {
            this._super(visibility,partner,clickpos);

            // Por ahora solo se permiten contactos de Colombia
            // TODO: Dar soporte para otros paises
            var country_select = $('.client-address-country');
            country_select.val("49");
            country_select.attr('disabled', true);
        },
    });

    screens.PaymentScreenWidget.include({
        finalize_validation: function () {
            var self = this;
            var order = this.pos.get_order();
            if (order.is_paid_with_cash() && this.pos.config.iface_cashdrawer) {

                this.pos.proxy.open_cashbox();
            }
            order.initialize_validation_date();
            order.finalized = true;
            if (order.is_to_invoice()) {
                var invoiced = this.pos.push_and_invoice_order(order);
                this.invoicing = true;

                invoiced.fail(this._handleFailedPushForInvoice.bind(this, order, false));

                invoiced.done(function (orderId) {
                    self.invoicing = false;
                    rpc.query({
                        model: 'pos.order',
                        method: 'get_invoice',
                        args: [orderId],
                    })
                    .then(function (invoice) {
                        var order = self.pos.get_order();
                        order.invoice = invoice;
                        self.gui.show_screen('receipt');
                    });
                });
            } else {
                this.pos.push_order(order);
                this.gui.show_screen('receipt');
            }
        },
    });

    module.PosModel = module.PosModel.extend({
        push_and_invoice_order: function (order) {
            var self = this;
            var invoiced = new $.Deferred();
            if (!order.get_client()) {
                invoiced.reject({ code: 400, message: 'Missing Customer', data: {} });
                return invoiced;
            }
            var order_id = this.db.add_order(order.export_as_JSON());

            this.flush_mutex.exec(function () {
                var done = new $.Deferred(); // holds the mutex

                // send the order to the server
                // we have a 30 seconds timeout on this push.
                // FIXME: if the server takes more than 30 seconds to accept the order,
                // the client will believe it wasn't successfully sent, and very bad
                // things will happen as a duplicate will be sent next time
                // so we must make sure the server detects and ignores duplicated orders

                var transfer = self._flush_orders([self.db.get_order(order_id)], { timeout: 30000, to_invoice: true });

                transfer.fail(function (error) {
                    invoiced.reject(error);
                    done.reject();
                });

                // on success, get the order id generated by the server
                transfer.pipe(function (order_server_id) {

                    // generate the pdf and download it
                    if (order_server_id.length) {
                        self.chrome.do_action('point_of_sale.pos_invoice_report', {
                            additional_context: {
                                active_ids: order_server_id,
                            }
                        }).done(function () {
                            invoiced.resolve(order_server_id);
                            done.resolve();
                        }).fail(function (error) {
                            invoiced.reject({ code: 401, message: 'Backend Invoice', data: { order: order } });
                            done.reject();
                        });
                    } else {
                        // The order has been pushed separately in batch when
                        // the connection came back.
                        // The user has to go to the backend to print the invoice
                        invoiced.reject({ code: 401, message: 'Backend Invoice', data: { order: order } });
                        done.reject();
                    }
                });
                return done;
            });
            return invoiced;
        },
    });
});
