# -*- coding: utf-8 -*-
{
    'name': 'Free POS electronic invoice for Colombia by Jorels',
    'category': 'Localization',
    'version': '12.0',
    'author': 'Jorels SAS',
    'license': 'LGPL-3',
    'maintainer': 'Jorels SAS',
    'website': 'https://www.jorels.com',
    'summary': 'Free POS electronic invoice for Colombia by Jorels',
    'description': """Free POS electronic invoice for Colombia by Jorels""",
    
    'depends': [
        'point_of_sale',
        'l10n_co_edi_jorels',
    ],
    'data': [
        'views/pos_view.xml',
    ],
    'qweb': ['static/src/xml/pos.xml'],
    'installable': True,
    'auto_install': True,
}
