# -*- coding: utf-8 -*-
#
# Jorels S.A.S. - Copyright (2019-2020)
#
# This file is part of l10n_co_edi_jorels.
#
# l10n_co_edi_jorels is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# l10n_co_edi_jorels is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with l10n_co_edi_jorels.  If not, see <https://www.gnu.org/licenses/>.
#
# email: info@jorels.com
#

from odoo import api, models


class PosOrder(models.Model):
    _inherit = 'pos.order'

    @api.multi
    def get_invoice(self):
        self.ensure_one()
        return {
            "number": self.invoice_id.number,
            "ei_uuid": self.invoice_id.ei_uuid,
            "ei_qr_data": self.invoice_id.ei_qr_data,
            "ei_is_valid": self.invoice_id.ei_is_valid,
            "resolution_resolution": self.invoice_id.journal_id.sequence_id.resolution_id.resolution_resolution,
            "resolution_resolution_date": self.invoice_id.journal_id.sequence_id.resolution_id.resolution_resolution_date,
            "resolution_prefix": self.invoice_id.journal_id.sequence_id.resolution_id.resolution_prefix,
            "resolution_from": self.invoice_id.journal_id.sequence_id.resolution_id.resolution_from,
            "resolution_to": self.invoice_id.journal_id.sequence_id.resolution_id.resolution_to,
            "resolution_date_from": self.invoice_id.journal_id.sequence_id.resolution_id.resolution_date_from,
            "resolution_date_to": self.invoice_id.journal_id.sequence_id.resolution_id.resolution_date_to,
        }
